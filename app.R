#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)
library(shinydashboard)
library(shinyWidgets)
library(MetNet) # mass diff network computation
library(igraph)
library(reshape2)
library(tidyverse)
library(GGally) # networkviz
library(network)
library(visNetwork)
library(ggsci)
library(DT)


mycss <- ".js-irs-0 .irs-single, .js-irs-0 .irs-bar-edge, .js-irs-0 .irs-bar {
                                                  background: green;
                                                  border-top: 1px solid #000039 ;
                                                  border-bottom: 1px solid green ;}
                           .irs-from, .irs-to, .irs-single { background: green }, 
                           .progress-bar{background-color: green;}
"

# Define UI for application that draws a histogram<
ui <- dashboardPage(skin = "green", title="CHaMP",
    # Application title
    dashboardHeader(title = span("CHAMP", style = "color: black; font-size: 14px; font-weight: bold;")),
    dashboardSidebar(
        tags$head(tags$style(HTML(mycss))),
        fluidRow(
            column(7,
                    span("Load a list of compound masses (ideally from twin-peaks analysis) and explore putative degradation or metabolization routes, using a mass difference network rooted toward a mass of interest.")
                   ),
            column(2,
                   HTML('<right><img src="CHaMP_logo.png" width="60"></right>')
            )
        ),
        fileInput("masses",
                  div(h3("Filtered masses"), h5("Upload a single column .txt file with one mass per line, using dot as decimal separator")),
                  accept = c("text/csv",
                             "text/comma-separated-values,text/plain",
                             ".csv")
        ),
        selectInput(
            "seed",
            label = h3("Mass of compound of interest"),
            choices = c()
        ),
        sliderInput("tolerance",
                    "ppm:",
                    min = 1,
                    max = 20,
                    value = 5
        ),
        menuItem(
            "Advanced settings",
            # "Upload a csv file with columns \"Name\", \"Formula difference\", \"mass\", and \"color\" (in Hex color code format)"
            checkboxInput("wholeNet", "Show whole network", value = FALSE, width = NULL),
            fileInput("CustomTransitions",
                      div("Upload custom transitions"),
                      accept = c("text/csv",
                                 "text/comma-separated-values,text/plain",
                                 ".csv")
            ),
            expandedName = "Advanced settings",
            startExpanded = FALSE
        )
    ),
    dashboardBody(
        tabItems(
            tabItem("dashboard", class = "active",
                fluidRow(
                    box(
                        tags$head(tags$style(HTML('.bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-danger,
                                       .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-danger {
                                  -webkit-box-shadow: none;
                                  border-color: #CCCCCC;
                                  background: #00a85a;
                                  box-shadow: none; 
                                  outline: none;
                                  }'))),
                        width = 10, solidHeader = TRUE, status="success",
                        title = "Degradation/Metabolization Network",
                        visNetworkOutput("myNet"),
                        switchInput(
                            "force",
                            label = "Node\nPosition",
                            value = TRUE,
                            onLabel = "Auto",
                            offLabel = "Manual",
                            onStatus = "danger",
                            size="mini"
                        )
                    ),
                    box(
                        width = 2, solidHeader = TRUE, collapsible = TRUE, status="success",
                        title = "Legend",
                        htmlOutput("myLegend")
                    ),
                    box(
                        tags$style(
                            ".bootstrap-select.show-tick .dropdown-menu .selected span.check-mark {left: 2px; right: auto !important}"
                        ),
                        width = 2, solidHeader = TRUE, collapsible = TRUE, collapsed=FALSE, status="success",
                        title = "Transition select",
                        uiOutput("mySelection"),
                        style = "height: 35vh; overflow-y: scroll"
                    ),
                    box(
                        width = 12, solidHeader = TRUE, collapsible = TRUE, status="success",
                        title = "Network Table",
                        dataTableOutput("myTab")     
                    )
                )
            )
        )
    )
)

# Define server logic required to draw a histogram
server <- function(input, output, session) {

        #network viz
        output$myNet <- renderVisNetwork({
            reactNet()
        })
        
        #network table
        output$myTab <- renderDataTable({
            outputTable()
        })
        
        #legend widget
        output$myLegend <- renderUI({
            HTML(paste(legend(), sep = '<br>'))
        })
        
        #get list of masses from file
        masses <- reactive({
            req(input$masses)
            m <- read.csv(input$masses$datapath, header = FALSE)
            #check file
            if(!is.numeric(m[1,1])){
                showModal(modalDialog(
                    title = "File format error",
                    "Please upload a valid file with one compound per row, and m/z value in first column",
                    easyClose = TRUE
                ))
                return(NULL)
            }
            data.frame(mz=m[,1])   
            
        })
        
        #select seed for subgraph extraction
        observe({
            s <- masses()$mz
            updateSelectInput(session, "seed", choices = s, selected = tail(s,1))
        })
        
        #transform mass list to node list
        nodes <- reactive({
            data.frame(id=row.names(masses()),v2=masses())
        })
        
        #upload list of known chemical transformations
        transformations <- reactive({
            #import transformation
            t <- read.csv("./data/transitions.csv")
            
            err <- "Please upload a valid csv file"
            if(isTruthy(input$CustomTransitions)){
                t2 <- read.csv(input$CustomTransitions$datapath, header = TRUE)
                
                #check file format
                flag <- FALSE
                if(ncol(t2)!=ncol(t)){
                    flag <- TRUE
                    err <- "Wrong table size. Please upload a valid csv file with columns \"Name\", \"Formula difference\", \"mass\", and \"color\" (in Hex color code format)"
                }else if(nrow(t2) == 0){
                    flag <- TRUE
                    err <- "Empty file. Please upload a valid csv file with columns \"Name\", \"Formula difference\", \"mass\", and \"color\" (in Hex color code format)"
                }
                else if(!all(colnames(t2) ==colnames(t))){
                    flag <- TRUE
                    err <- "Wrong column names. Please upload a valid csv file with columns \"Name\", \"Formula difference\", \"mass\", and \"color\" (in Hex color code format)"
                }else if(!is.character(t2[1,1])){
                    flag <- TRUE
                    err <- "Wrong Name format. Please upload a valid csv file"
                }else if(!is.numeric(t2[1,3])){
                    flag <- TRUE
                    err <- "Wrong mass difference format, should be numeric. Please upload a valid csv file"
                }else if(!grepl("^#(([0-9a-fA-F]{2}){3}|([0-9a-fA-F]){3})$",t2[1,4])){
                    flag <- TRUE
                    err <- "Wrong color format, should be Hex color code Please upload a valid csv file"
                }
                
                if(flag){
                    showModal(modalDialog(
                        title = "File format error",
                        err,
                        easyClose = TRUE
                    ))
                }else{
                    t <- rbind(t,t2)
                }
                
            }
            
            #set label color according to color brightness
            hcl <- farver::decode_colour(t$color, "rgb", "hcl")
            t$textCol <- ifelse(hcl[, "l"] > 50, "black", "white")
            t %>% rename("group" = Name, "formula" = Formula.difference) %>%
              mutate(across(mass, as.numeric))
        })
        
        
        #select transitions
        output$mySelection <- renderUI({
            selection <- transformations()$group %>% sort()
            pickerInput(inputId="selection", label="",
                               choices=selection,   
                                options = list(
                                   `actions-box` = TRUE, 
                                   size = FALSE,
                                   `selected-text-format` = "count",
                                   `count-selected-text` = "{0}/{1} selected",
                                   dropupAuto=TRUE,
                                   showTick = TRUE
                                ), 
                                multiple = TRUE,
                                width = "fit",
                                selected = selection)
        })
        
        selectedTransformation <- reactive({
            ft = transformations()
            ft %>% filter(group %in% input$selection)
        })
        
        #get edge list: compute mass difference and match against transformation lists
        # use ppm tolerance for matching
        edges <- reactive({
            struct_adj <- data.frame(matrix(ncol=6,nrow=0, dimnames=list(NULL, c("Row","Col","binary","group","formula","mass"))))
            if(nrow(selectedTransformation()) != 0){
                struct_adj <- structural(x = masses(), 
                                         transformation = selectedTransformation(), 
                                         ppm = input$tolerance, 
                                         directed = TRUE, 
                                         var = c("group","formula","mass"))

                struct_adj <- struct_adj %>% as.data.frame() %>%
                    filter(binary == 1) %>%  # Keep matches only
                    mutate(source_mz = masses()[Row,],
                           target_mz = masses()[Col,],
                           delta = abs(source_mz - target_mz)) %>%
                    left_join(., select(selectedTransformation(), c(group,color)), by = "group")
            }
            struct_adj
        })
        
        #create legend, selecting only transformations present in graph
        legend <- reactive({
            if(gsize(graph())>0){
                #select transformations
                legend <- filter(selectedTransformation(), selectedTransformation()$group %in% E(graph())$group)
                txt <- paste("<span style=\"background-color:",legend$color,";color:",legend$textCol,"\";>",legend$group,"</span>", sep="")
            }
        })
        
        # build the graph from the edge list and node list
        # compute connected components
        # select seed's component only
        graph <- reactive({
            #build graph and set edges&nodes attributes
            g <- igraph::graph_from_data_frame(edges(), directed = TRUE, vertices = nodes())
            E(g)$name <- edges()$group
            E(g)$label <- edges()$formula
            V(g)$color <- "lightgray" #set node color
            V(g)$label <- V(g)$mz
            
            
            V(g)$color[V(g)$mz==input$seed] <- "red" #set seed node color
            g2 <- g
            if(input$wholeNet==FALSE){
                #compute connected components
                cc <- components(g, mode = "weak")
                #extract seed cc
                c <- cc$membership[V(g)[mz==input$seed]] #compute connected component
                g2 <- subgraph(g,V(g)[cc$membership==c]) #extract subgraph
                V(g)$cc <- (cc$membership==c)
            }
            g2
        })
        
        #create network table
        outputTable <- reactive({
            tbl <- select(edges(),-Row,-Col,-color,-binary)
            #check if compound is in extracted subnetwork
            tbl$seed_connected <- edges()$Row %in% V(graph())$name
            datatable(
                tbl, 
                filter = 'top', 
                colnames = c(
                    "Transformation"="group",
                    "mass #1"="source_mz",
                    "mass #2"="target_mz",
                    "Theoretical\nmass shift"="mass",
                    "mass shift" = "delta",
                    "Connected to\ncompound of interest"="seed_connected"),
                extensions = 'Buttons', options = list(
                    dom = 'flrtpBi',
                    pageLength = 50, 
                    buttons = list(
                            list(
                                extend = "copy",
                                text="Copy Table"
                            ),
                            list(
                                extend = "excel",
                                text="Save Table",
                                title=paste("table_",input$seed,"_",input$tolerance,"ppm", sep = "")
                            )
                       )
                    )
                ) %>% formatStyle(
                    'Transformation',
                    backgroundColor = styleEqual(selectedTransformation()$group,selectedTransformation()$color),
                    color = styleEqual(selectedTransformation()$group,selectedTransformation()$textCol),
                )
        })
        
        prev <- reactiveValues(coord = NULL)
        observe({
            invalidateLater(1000)
            visNetworkProxy("myNet") %>% visGetPositions(input = "myNodes") 
            if(!is.null(input$myNodes)){
                df <- do.call(rbind.data.frame, input$myNodes)
                df$name <- rownames(df)
                prev$coord <- df
            }
        })
        
        #create network viz, set aesthetic parameters
        reactNet = eventReactive(list(input$seed,input$tolerance, input$selection, input$wholeNet, input$force, input$CustomTransitions), {

            #visualize network
            vis.nodes = igraph::as_data_frame(graph(),what = "vertices") %>%
            mutate(id = name, label = as.character(mz), size = 12,
                   borderWidth = 1, color.border = "black") %>%
            rename(color.background = color)
            
            #store previous node coords
            if(!is.null(prev$coord)){
                vis.nodes <- merge(x = vis.nodes, y = prev$coord, by="name",all.x=TRUE) 
            }
            
            #set links if seed not isolated
            if(gsize(graph())>0){
              vis.links = igraph::as_data_frame(graph(),what = "edges") %>%
                mutate(arrows = "to", title = group, label = "", width = 4)
            }
            
            #set canvas size, add options (export, interaction buttons)
            net <- visNetwork(vis.nodes, vis.links, height = "900px", width = "900px")  %>%
                visLayout(randomSeed = 42) %>%
                visExport(type = "png", name = paste("network_",input$seed,"_",input$tolerance,"ppm", sep = ""), 
                          float = "left", label = "Save network", background = "transparent", style= "") %>%
                visOptions(highlightNearest = list(enabled = T, degree = 0)) %>%
                visInteraction(navigationButtons = TRUE) 
            if(input$force == FALSE){
                net <- net %>% 
                    visNodes(physics = FALSE)
            }
            
            net
            
        })
}

# Run the application 
shinyApp(ui = ui, server = server)