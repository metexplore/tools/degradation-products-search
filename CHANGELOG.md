# Version 1.1

## feature
- Transition selection : allow to ignore specific transitions in network
- Manual node position control : freeze the network physics to enable fine node positioning
- Whole Network view : Option to render the full network, including nodes not connected to the compound of interest
- Custom transitions: import a transitions file that will expand the default transition list

## fix
- Change decimal separator in mass list: dot preferred over the comma
- Persistent node position: Nodes coordinates kept after transitions selection and node position control switch


# Version 1.0

Original release

## feature
- Mass list upload
- Mass-difference network computation with connected component filtering from a single mass selection
- Dynamic network visualization
- Full network table with filters and Excel export
