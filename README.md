
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7912853.svg)](https://doi.org/10.5281/zenodo.7912853)


# CHaMP 
**C**an I **Ha**ve **M**etabolites, **P**lease?

## Description
CHaMP is an RShiny app that loads a list of compound masses from twin-peaks analysis, and build a mass difference network from which is extracted the connected component containing a mass of interest. It provides a network visualization of the sub-network that encompass potential metabolic and spontaneous reactions that leads to degradation/metabolization products.

## Usage
```
R -e "shiny::runApp('app.R')"
```

## Contributing
Pull requests are welcome **on the gitlab repo** ([forgemia.inra.fr/metexplore/tools/degradation-products-search](https://forgemia.inra.fr/metexplore/tools/degradation-products-search)). For major changes, please open an issue first to discuss what you would like to change.  


## Authors and acknowledgment
please cite:

> Clément Frainay, Marine Valleix, Jean-François Martin, Elodie Person, Laurent Debrauwer, Sylvie Chevolleau, Daniel Zalko, & Fabien Jourdan. (2023). CHaMP : an interactive tool to infer biotransformation pathways from LC-MS data (1.1.1). Zenodo. https://doi.org/10.5281/zenodo.7912853


## License
CHaMP is distributed under the open license [CeCILL-2.1](https://cecill.info/licences/Licence_CeCILL_V2.1-en.html) (compatible GNU-GPL).  


